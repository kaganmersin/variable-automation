
# GitLab CI/CD: Basic Docker Image Builder

A GitLab CI/CD script to build and push a Docker image to DockerHub whenever changes are merged into the `main` branch. The Docker image's tag will contain the branch's slugified name and the first six characters of the commit SHA.

## Prerequisites

- A GitLab account with a repository containing your code.
- [Docker](https://www.docker.com/) installed and configured on your CI/CD runner.
- A [DockerHub](https://hub.docker.com/) account where the Docker images will be pushed to.
  
## Setup & Usage

1. Setup Environment Variables in GitLab:
   Navigate to your GitLab project > Settings > CI/CD > Variables. Here, you will need to add:
   - `DOCKER_USERNAME`: Your DockerHub username.
   - `DOCKER_PASSWORD`: Your DockerHub password or access token.

![secrets.png](../assets/images/secrets.png)

2. Commit and push this script into the `.gitlab-ci.yml` file in your GitLab repository root.

3. Whenever you merge changes into the `main` branch, GitLab CI/CD will automatically trigger, building a Docker image and pushing it to DockerHub. The image will be tagged with the merging branch's slugified name and the first six characters of the commit SHA.

![job.png](../assets/images/job.png)

## How it Works

1. The script listens for changes to the `main` branch.
2. When changes are detected, it calculates the desired tag based on the branch name and the commit SHA.

![tags.png](../assets/images/tags.png)

3. It then logs into DockerHub using the provided environment variables.
4. A Docker image is built using the latest changes.
5. This image is then pushed to DockerHub with the computed tag.

![push.png](../assets/images/push.png)

6. After pushing, it logs out from DockerHub.

---

**Note**: It's essential to keep your `DOCKER_PASSWORD` secure. Always prefer using access tokens instead of the actual password. Ensure you are familiar with the naming conventions and restrictions of Docker image tags to avoid any issues.