# Use the official Nginx image as a base
FROM nginx:alpine

# Copy our static HTML file to the Nginx web root
COPY ./index.html /usr/share/nginx/html/index.html
